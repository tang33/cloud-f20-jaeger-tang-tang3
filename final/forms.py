import os
from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, SelectField
from wtforms.validators import DataRequired, Optional, Length
from google.cloud import texttospeech


class TextToSpeechForm(FlaskForm):
    """
    Create user form for submitting text for speech synthesis
    """

    # set gcloud environment credentials
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'cloud-f20-jaeger-tang-tang3-72758f6822e4.json'

    # Instantiates a client
    client = texttospeech.TextToSpeechClient()

    # Performs the list voices request
    voices = client.list_voices()

    # Get language list
    voice_codes_list = list(dict.fromkeys([voice.language_codes[0] for voice in voices.voices]))
    language_list = [(ind + 1, voice) for ind, voice in enumerate(voice_codes_list)]

    # Get voice gender
    voice_gender = [(1, "Male"), (2, "Female")]

    text_field = TextAreaField('Reminder:', validators=[DataRequired(), Length(max=1400)], default="Look away from the screen, raise your arms, and take 3 deep breaths.")
    language_options = SelectField('Language:', validators=[Optional()],
            choices=language_list, default=8)
    gender_options = SelectField('Voice Gender:', validators=[Optional()],
            choices=voice_gender, default=1)
    submit = SubmitField('Convert Text to Speech')
