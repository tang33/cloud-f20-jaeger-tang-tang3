import json
import os

from google.cloud import texttospeech
from google.cloud import storage
from google.cloud import datastore

from flask import Flask, request, render_template, flash, redirect, send_file, url_for, session
from flask_bootstrap import Bootstrap

from forms import TextToSpeechForm

from datetime import datetime


# Use app for GCP
app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config['SECRET_KEY'] = 'tang3'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'cloud-f20-jaeger-tang-tang3-72758f6822e4.json'

CLOUD_STORAGE_BUCKET = "tang3-final-project" 

@app.route('/', methods=['GET', 'POST'])
def home():
    """
    Route to display home page and form to receive text from user for speech synthesis.
    """
    form = TextToSpeechForm()

    # Instantiates a client
    client = texttospeech.TextToSpeechClient()

    # Get the language list
    voices = client.list_voices()

    voice_codes_list = list(dict.fromkeys([voice.language_codes[0] for voice in voices.voices]))
    language_list = [(ind + 1, voice) for ind, voice in enumerate(voice_codes_list)]

    if request.method == 'POST':
        lang = dict(language_list).get(int(form.language_options.data))
        gender = dict([(1, texttospeech.SsmlVoiceGender.MALE),
            (2, texttospeech.SsmlVoiceGender.FEMALE)]).get(int(form.gender_options.data))
        messages = json.dumps({'text': form.text_field.data,
            'language': lang,
            'gender': gender})
        
        activity = int(request.form["activity"])
        rest = int(request.form["rest"])
        sets = int(request.form["sets"])

        session["activity"] = activity
        session["rest"] = rest
        session["sets"] = sets
        session["set_counter"] = 0
        
        return redirect(url_for('.synthesize', messages=messages))
    return render_template('home.jinja2', form=form)

@app.route('/synthesize')
def synthesize():
    """
    Route to synthesize speech using Google Text-to-Speech API.
    """

    # Create a Cloud Storage client.
    storage_client = storage.Client()

    # Get the bucket that the file will be uploaded to.
    bucket = storage_client.get_bucket(CLOUD_STORAGE_BUCKET)

    # Get requested text
    messages = json.loads(request.args['messages'])

    # Instantiates a client
    client = texttospeech.TextToSpeechClient()

    # Set the text input to be synthesized
    synthesis_input = texttospeech.SynthesisInput(text=messages['text'])

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.VoiceSelectionParams(
            language_code=messages['language'],
            ssml_gender=messages['gender'])

    # Select the type of audio file you want returned
    audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.MP3,
            effects_profile_id=["large-home-entertainment-class-device"])

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    #response = client.synthesize_speech(synthesis_input, voice, audio_config)
    response = client.synthesize_speech(
            input=synthesis_input, voice=voice, audio_config=audio_config
            )

    # Fetch the current date / time. UNIX time for file naming, and float
    # datetime for timestamp
    current_datetime = datetime.now()
    timestr = current_datetime.strftime("%Y%m%d-%H%M%S")

    # Create audio file name
    file_name= "output-" + timestr + ".mp3"
    session["file_name"] = file_name

    # Create a new blob and upload the file's content.
    blob = bucket.blob(file_name)
    blob.upload_from_string(response.audio_content, content_type="audio/mpeg")

    # Make the blob publicly viewable.
    blob.make_public()

    # Generate url to Cloud Bucket audio file output-%Y%m%d-%H%M%S.mp3
    session["source_url"] = "https://storage.googleapis.com/" + CLOUD_STORAGE_BUCKET + "/" + blob.name

    # Create a Cloud Datastore client.
    datastore_client = datastore.Client()

    # The kind for the new entity.
    kind = "Interrupts"

    # The name/ID for the new entity.
    name = blob.name

    # Create the Cloud Datastore key for the new entity.
    key = datastore_client.key(kind, name)

    # Construct the new entity using the key. Set dictionary values for entity
    # keys blob_name, storage_public_url, timestamp, and joy.
    entity = datastore.Entity(key)
    entity["blob_name"] = blob.name
    entity["audio_public_url"] = session["source_url"]
    entity["timestamp"] = current_datetime
    entity["speech"] = messages

    print(f"entiry: {entity}")

    # Save the new entity to Datastore.
    datastore_client.put(entity)

    """
    # The response's audio_content is binary.
    with open('./static/' + file_name, 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file "output.mp3"')

    return render_template("exercise.jinja2", activity=session["activity"],
            file_name=session["file_name"], source_url=session["source_url"])
    """
    return render_template("alert.jinja2", rest=session["rest"])


@app.route("/alert")
def alert():
    return render_template("alert.jinja2", rest=session["rest"])

@app.route("/rest")
def rest():
    return render_template("rest.jinja2", rest=session["rest"])


@app.route("/exercise")
def exercise():

    if session["set_counter"] == session["sets"]:
        return redirect(url_for("completed"))
    session["set_counter"] += 1
    return render_template("exercise.jinja2", activity=session["activity"],
            file_name=session["file_name"], source_url=session["source_url"])

@app.route("/complete")
def completed():
    return render_template("complete.jinja2", sets=session["set_counter"])

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
