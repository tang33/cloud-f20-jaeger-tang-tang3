from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import sslistmodel

class Add_new(MethodView):
    def get(self):
        return render_template('add_new.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = sslistmodel.get_model()
        model.insert(request.form['name'], request.form['description'], request.form['street_add'], request.form['type_of_service'], request.form['phone_no'], request.form['hours'], request.form['reviews'])
        return redirect(url_for('index'))
