# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        [ name, description, street_add, ... ]
    where name, description ... are Python strings
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return
[entity['name'],entity['description'],entity['street_add'],entity['type_of_service'],entity['phone_no'],entity['hours'],entity['reviews']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-f20-jaeger-tang-tang3')

    def select(self):
        query = self.client.query(kind = 'SS_Entry')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(self, name, description, street_add, type_of_service, phone_no,
            hours, reviews):
        key = self.client.key('SS_Entry')
        rev = datastore.Entity(key)
        rev.update( {
            'name': name,
            'description' : email,
            'street_add' : street_add,
            'type_of_service' : type_of_service,
            'phone_no' : phone_no,
            'hours' : hours,
            'reviews' : reviews,
            })
        self.client.put(rev)
        return True
