class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, description, street_add, type_of_service, phone_no, hours, reviews):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param street_add: String
        :param type_of_service: String
        :param phone_no: Integer
        :param hours: String
        :param reviews: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
