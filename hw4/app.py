"""
A flask app that records social service resources.
"""
import flask, os
from flask.views import MethodView
from index import Index
from add_new import Add_new

app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

app.add_url_rule('/add_new/',
                 view_func=Add_new.as_view('add_new'),
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=int(os.environ.get('PORT',5000)))
