from flask import render_template
from flask.views import MethodView
import sslistmodel

class Index(MethodView):
    def get(self):
        model = sslistmodel.get_model()
        entries = [dict(name=row[0], description=row[1], street_add=row[2], type_of_service=row[3], phone_no=row[4], hours=row[5], reviews=row[6]) for row in model.select()]
        return render_template('index.html',entries=entries)
