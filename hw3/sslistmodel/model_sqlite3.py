"""
A simple social services list flask app.

This database be created with the following SQL (see bottom of this file):

    create table sslist (name text, description text, street_add text, type_of_service text, phone_no, hours, reviews)

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from sslist")
        except sqlite3.OperationalError:
            cursor.execute("create table sslist (name text, description text, street_add text, type_of_service text, phone_no integer, hours blob, reviews blob)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, street_add, type_of_service, phone_no, hours, reviews
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM sslist")
        return cursor.fetchall()

    def insert(self, name, description, street_add, type_of_service, phone_no, hours, reviews):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param street_add: String
        :param type_of_service: String
        :param phone_no: String
        :param hours: String
        :param reviews: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'description':description, 'street_add':street_add, 'type_of_service':type_of_service, 'phone_no':phone_no, 'hours':hours, 'reviews':reviews}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into sslist (name, description, street_add, type_of_service, phone_no, hours, reviews) VALUES (:name, :description, :street_add, :type_of_service, :phone_no, :hours, :reviews)", params)

        connection.commit()
        cursor.close()
        return True
